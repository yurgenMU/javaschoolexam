package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] matrix;
        if (!isValid(inputNumbers))
            throw new CannotBuildPyramidException();
        else {
            int height = (int) ((Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2);
            int width = height * 2 - 1;
            Collections.sort(inputNumbers);
            matrix = new int[height][width];
            Iterator<Integer> iterator = inputNumbers.iterator();
            int leftBorder = width / 2;
            int rightBorder = width / 2;
            while (iterator.hasNext()) {
                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        if ((j >= leftBorder) && (j <= rightBorder)) {
                            matrix[i][j] = iterator.next();
                            j++;
                        }
                    }
                    leftBorder--;
                    rightBorder++;
                }
            }
        }
        return matrix;
    }


    private boolean isValid(List<Integer> input) {
        if (input.contains(null))
            return false;
        double n = (Math.sqrt(1 + 8 * input.size()) - 1) / 2;
        if (n != (int) n)
            return false;
        return true;
    }

}
