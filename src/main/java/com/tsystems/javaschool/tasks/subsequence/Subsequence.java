package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     *          y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if ((x == null) || (y == null))
            throw new IllegalArgumentException();
        if(x.isEmpty())
            return true;
        Iterator iteratorX = x.iterator();
        Iterator iteratorY = y.iterator();
        Object a = iteratorX.next();
        while (iteratorY.hasNext()) {
            Object b = iteratorY.next();
            if (b.equals(a)) {
                if (iteratorX.hasNext())
                    a = iteratorX.next();
            }
        }
        if (!iteratorX.hasNext())
            return true;
        return false;
    }
}
