package com.tsystems.javaschool.tasks.calculator;


import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Converting arithmetic expression into a reverse Polish notation
 */

public class Converter {

    public String getNotation(String s) {
        StringBuilder ans = new StringBuilder();
        Stack<Character> stack = new Stack<>();
        if (s.charAt(0) == '-')
            ans.append("0 ");
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (isDigit(c))
                ans.append(c);
            if (c == '.')
                ans.append(c);
            if (isOperator(c)) {
                ans.append(' ');
                if (stack.isEmpty()) {
                    stack.push(c);
                } else {
                    while ((!stack.empty()) && (getOperandPriority(c) <= getOperandPriority(stack.peek()))) {
                        ans.append(' ').append(stack.pop()).append(' ');
                    }
                    stack.push(c);
                }
            }
            if (c == '(')
                stack.push(c);
            if (c == ')') {
                while (stack.peek() != '(') {
                    ans.append(' ').append(stack.pop()).append(' ');
                }
                stack.pop();
            }
        }
        for (int j = stack.size() - 1; j >= 0; j--) {

            ans.append(' ').append(stack.get(j)).append(' ');

        }
        return ans.toString();
    }


    private int getOperandPriority(char operand) {
        int ans = 0;
        if ((operand == '(') || (operand == ')'))
            ans = 0;
        if ((operand == '+') || (operand == '-'))
            ans = 1;
        if ((operand == '*') || operand == '/')
            ans = 2;
        return ans;
    }

    /**
     * Checks whether actual character is digit
     *
     * @param c checked character
     * @return
     */
    private boolean isDigit(char c) {
        boolean contains = false;
        char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        for (char digit : digits) {
            if (digit == c) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    /**
     * Checks whether actual character is operator
     *
     * @param c checked character
     * @return
     */
    private boolean isOperator(char c) {
        boolean contains = false;
        char[] operators = {'-', '+', '*', '/'};
        for (char operator : operators) {
            if (operator == c) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    /**
     * Checks whether our String is valid
     *
     * @param s Required string
     * @return true if valid
     */
    public boolean isValid(String s) {
        if (s.equals(""))
            return false;
        boolean valid = true;
        Character[] operators = {'.', '-', '+', '*', '/'};
        int leftBracketCount = 0;
        int rightBracketCount = 0;
        List<Character> operatorList = Arrays.asList(operators);
        Character[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        List<Character> digitList = Arrays.asList(digits);
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if ((i >= 1) && (operatorList.contains(ch)) && (operatorList.contains(s.charAt(i - 1)))) {
                valid = false;
                break;
            }
            if ((!operatorList.contains(ch)) && (!digitList.contains(ch)) && (ch != '(') && (ch != ')'))
                valid = false;
            if ((ch == '=') && (i != s.length() - 1)) {
                valid = false;
                break;
            }
            if (ch == ')')
                rightBracketCount++;
            if (ch == '(')
                leftBracketCount++;

        }
        if (rightBracketCount != leftBracketCount) {
            valid = false;
        }
        return valid;
    }
}
