package com.tsystems.javaschool.tasks.calculator;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Stack;
import java.util.stream.Collectors;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public static String evaluate(String statement) {
        Converter converter = new Converter();
        if ((statement == null) || (!converter.isValid(statement))) {
            return null;
        }
        Stack<Double> stack = new Stack<>();
        List<String> tokens = Arrays.stream(converter.getNotation(statement).split(" "))
                .filter(s -> !s.equals(""))
                .collect(Collectors.toList());
        for (String s : tokens) {
            double a;
            double b;
            switch (s) {
                case "+":
                    a = stack.pop();
                    b = stack.pop();
                    stack.push(a + b);
                    break;
                case "-":
                    a = stack.pop();
                    b = stack.pop();
                    stack.push(b - a);
                    break;
                case "*":
                    a = stack.pop();
                    b = stack.pop();
                    stack.push(a * b);
                    break;
                case "/":
                    a = stack.pop();
                    b = stack.pop();
                    stack.push(b / a);
                    break;
                default:
                    stack.push(Double.parseDouble(s));
                    break;
            }
        }
        double x = stack.pop();
        if (String.valueOf(x).equals("Infinity"))
            return null;
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat df = new DecimalFormat("#.####", otherSymbols);
        return df.format(x);
    }

}
